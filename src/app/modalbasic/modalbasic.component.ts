import { Component, Input } from '@angular/core';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalcontentComponent} from '../modalcontent/modalcontent.component'
import { ListingService } from '../listing.service';

@Component({
  selector: 'ngbd-modal-component',
  templateUrl: './modalbasic.component.html'
})
export class NgbdModalComponent {
  constructor(private modalService: NgbModal, private listingService: ListingService) {}

  open(id: number) {
    const modalRef = this.modalService.open(ModalcontentComponent);
    
    modalRef.componentInstance.name = this.listingService.getName(id);
    modalRef.componentInstance.lookupId = id
    modalRef.componentInstance.listOfNames = this.listingService.getNames();
  
   
  }
}