import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { NgbdModalComponent } from './modalbasic/modalbasic.component';
import { ModalcontentComponent } from './modalcontent/modalcontent.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap'
import {UserprofileModule} from 'userprofile'
import {UserprofileComponent} from 'userprofile'
@NgModule({
  declarations: [
    AppComponent,

    NgbdModalComponent,
    ModalcontentComponent
 

   
  ],
  imports: [
    BrowserModule, NgbModule,UserprofileModule
  ],
  entryComponents: [ModalcontentComponent, UserprofileComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
