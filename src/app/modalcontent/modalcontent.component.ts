import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './modalcontent.component.html',
  styleUrls: ['./modalcontent.component.css']
})
export class ModalcontentComponent {

  @Input() name;
  @Input() listOfNames: string[];
  @Input() lookupId : number;

  constructor(public activeModal: NgbActiveModal) { }



}
