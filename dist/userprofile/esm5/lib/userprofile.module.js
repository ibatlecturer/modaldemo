/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { UserprofileComponent } from './userprofile.component';
var UserprofileModule = /** @class */ (function () {
    function UserprofileModule() {
    }
    UserprofileModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [UserprofileComponent],
                    exports: [UserprofileComponent]
                },] }
    ];
    return UserprofileModule;
}());
export { UserprofileModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnByb2ZpbGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdXNlcnByb2ZpbGUvIiwic291cmNlcyI6WyJsaWIvdXNlcnByb2ZpbGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7OztnQkFFOUQsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxFQUNSO29CQUNELFlBQVksRUFBRSxDQUFDLG9CQUFvQixDQUFDO29CQUNwQyxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEM7OzRCQVJEOztTQVNhLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVc2VycHJvZmlsZUNvbXBvbmVudCB9IGZyb20gJy4vdXNlcnByb2ZpbGUuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtVc2VycHJvZmlsZUNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtVc2VycHJvZmlsZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVXNlcnByb2ZpbGVNb2R1bGUgeyB9XG4iXX0=