(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('userprofile', ['exports', '@angular/core'], factory) :
    (factory((global.userprofile = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var UserprofileService = /** @class */ (function () {
        function UserprofileService() {
        }
        UserprofileService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        UserprofileService.ctorParameters = function () { return []; };
        /** @nocollapse */ UserprofileService.ngInjectableDef = i0.defineInjectable({ factory: function UserprofileService_Factory() { return new UserprofileService(); }, token: UserprofileService, providedIn: "root" });
        return UserprofileService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var UserprofileComponent = /** @class */ (function () {
        function UserprofileComponent() {
        }
        /**
         * @return {?}
         */
        UserprofileComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        UserprofileComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-userprofile',
                        template: "\n    <p>\n      userprofile works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        UserprofileComponent.ctorParameters = function () { return []; };
        return UserprofileComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var UserprofileModule = /** @class */ (function () {
        function UserprofileModule() {
        }
        UserprofileModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [UserprofileComponent],
                        exports: [UserprofileComponent]
                    },] }
        ];
        return UserprofileModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.UserprofileService = UserprofileService;
    exports.UserprofileComponent = UserprofileComponent;
    exports.UserprofileModule = UserprofileModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnByb2ZpbGUudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly91c2VycHJvZmlsZS9saWIvdXNlcnByb2ZpbGUuc2VydmljZS50cyIsIm5nOi8vdXNlcnByb2ZpbGUvbGliL3VzZXJwcm9maWxlLmNvbXBvbmVudC50cyIsIm5nOi8vdXNlcnByb2ZpbGUvbGliL3VzZXJwcm9maWxlLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJwcm9maWxlU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi11c2VycHJvZmlsZScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICB1c2VycHJvZmlsZSB3b3JrcyFcbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgVXNlcnByb2ZpbGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVzZXJwcm9maWxlQ29tcG9uZW50IH0gZnJvbSAnLi91c2VycHJvZmlsZS5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1VzZXJwcm9maWxlQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW1VzZXJwcm9maWxlQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVc2VycHJvZmlsZU1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6WyJJbmplY3RhYmxlIiwiQ29tcG9uZW50IiwiTmdNb2R1bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQU9FO1NBQWlCOztvQkFMbEJBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O2lDQUpEOzs7Ozs7O0FDQUE7UUFhRTtTQUFpQjs7OztRQUVqQix1Q0FBUTs7O1lBQVI7YUFDQzs7b0JBZEZDLFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixRQUFRLEVBQUUsbURBSVQ7cUJBRUY7Ozs7bUNBVkQ7Ozs7Ozs7QUNBQTs7OztvQkFHQ0MsV0FBUSxTQUFDO3dCQUNSLE9BQU8sRUFBRSxFQUNSO3dCQUNELFlBQVksRUFBRSxDQUFDLG9CQUFvQixDQUFDO3dCQUNwQyxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztxQkFDaEM7O2dDQVJEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9