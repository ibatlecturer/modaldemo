/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { UserprofileComponent } from './userprofile.component';
export class UserprofileModule {
}
UserprofileModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [UserprofileComponent],
                exports: [UserprofileComponent]
            },] }
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnByb2ZpbGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdXNlcnByb2ZpbGUvIiwic291cmNlcyI6WyJsaWIvdXNlcnByb2ZpbGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBUS9ELE1BQU07OztZQU5MLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFDUjtnQkFDRCxZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEMsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVXNlcnByb2ZpbGVDb21wb25lbnQgfSBmcm9tICcuL3VzZXJwcm9maWxlLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbVXNlcnByb2ZpbGVDb21wb25lbnRdLFxuICBleHBvcnRzOiBbVXNlcnByb2ZpbGVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJwcm9maWxlTW9kdWxlIHsgfVxuIl19