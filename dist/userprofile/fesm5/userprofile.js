import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var UserprofileService = /** @class */ (function () {
    function UserprofileService() {
    }
    UserprofileService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    UserprofileService.ctorParameters = function () { return []; };
    /** @nocollapse */ UserprofileService.ngInjectableDef = defineInjectable({ factory: function UserprofileService_Factory() { return new UserprofileService(); }, token: UserprofileService, providedIn: "root" });
    return UserprofileService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var UserprofileComponent = /** @class */ (function () {
    function UserprofileComponent() {
    }
    /**
     * @return {?}
     */
    UserprofileComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    UserprofileComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-userprofile',
                    template: "\n    <p>\n      userprofile works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    UserprofileComponent.ctorParameters = function () { return []; };
    return UserprofileComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var UserprofileModule = /** @class */ (function () {
    function UserprofileModule() {
    }
    UserprofileModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [UserprofileComponent],
                    exports: [UserprofileComponent]
                },] }
    ];
    return UserprofileModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { UserprofileService, UserprofileComponent, UserprofileModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnByb2ZpbGUuanMubWFwIiwic291cmNlcyI6WyJuZzovL3VzZXJwcm9maWxlL2xpYi91c2VycHJvZmlsZS5zZXJ2aWNlLnRzIiwibmc6Ly91c2VycHJvZmlsZS9saWIvdXNlcnByb2ZpbGUuY29tcG9uZW50LnRzIiwibmc6Ly91c2VycHJvZmlsZS9saWIvdXNlcnByb2ZpbGUubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVXNlcnByb2ZpbGVTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLXVzZXJwcm9maWxlJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIHVzZXJwcm9maWxlIHdvcmtzIVxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBVc2VycHJvZmlsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVXNlcnByb2ZpbGVDb21wb25lbnQgfSBmcm9tICcuL3VzZXJwcm9maWxlLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbVXNlcnByb2ZpbGVDb21wb25lbnRdLFxuICBleHBvcnRzOiBbVXNlcnByb2ZpbGVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJwcm9maWxlTW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0lBT0U7S0FBaUI7O2dCQUxsQixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7Ozs2QkFKRDs7Ozs7OztBQ0FBO0lBYUU7S0FBaUI7Ozs7SUFFakIsdUNBQVE7OztJQUFSO0tBQ0M7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixRQUFRLEVBQUUsbURBSVQ7aUJBRUY7Ozs7K0JBVkQ7Ozs7Ozs7QUNBQTs7OztnQkFHQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQ3BDLE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO2lCQUNoQzs7NEJBUkQ7Ozs7Ozs7Ozs7Ozs7OzsifQ==