import { NgModule } from '@angular/core';
import { UserprofileComponent } from './userprofile.component';

@NgModule({
  imports: [
  ],
  declarations: [UserprofileComponent],
  exports: [UserprofileComponent]
})
export class UserprofileModule { }
