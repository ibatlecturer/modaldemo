/*
 * Public API Surface of userprofile
 */

export * from './lib/userprofile.service';
export * from './lib/userprofile.component';
export * from './lib/userprofile.module';
